import { Component, OnInit, Input, HostBinding } from '@angular/core';

@Component({
  selector: 'app-issue-header-row',
  templateUrl: './issue-header-row.component.html',
  styleUrls: ['./issue-row.component.scss']
})
export class IssueHeaderRowComponent implements OnInit {
  @Input() issue: any;
  @HostBinding('class') cssClass: string;
  constructor() {
    this.cssClass = 'header-row';
  }

  ngOnInit() {
  }

}
