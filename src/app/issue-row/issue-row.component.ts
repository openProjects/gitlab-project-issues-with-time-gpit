import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-issue-row',
  templateUrl: './issue-row.component.html',
  styleUrls: ['./issue-row.component.scss']
})
export class IssueRowComponent implements OnInit {
  @Input() issue: any;
  @Input() showDescription: boolean;
  @Input() showEstimated: boolean;
  @Input() showSpent: boolean;
  @Input() showRemaining: boolean;
  @Input() issueBaseUrl: string;

  get issueUrl() {
    return this.issueBaseUrl + this.issue.iid;
  }

  constructor() { }

  ngOnInit() {
  }

  getTimeRemaining() {
    if (this.issue.time_stats.time_spent) {
      return this.issue.time_stats.time_estimate - this.issue.time_stats.time_spent;
    }
    return this.issue.time_stats.time_estimate;
  }

}
