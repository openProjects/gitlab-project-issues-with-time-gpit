import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'secondsToHumanReadable'
})
export class SecondsToHumanReadablePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if(!isNaN(value)) {
      return value/60/60 + " hrs";
    }
    else if (isNaN(value)) {
      return "N/A"
    }
    return value;
  }

}
