import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-column-selector',
  templateUrl: './column-selector.component.html',
  styleUrls: ['./column-selector.component.scss']
})
export class ColumnSelectorComponent implements OnInit {
  @Output() columnsUpdated: EventEmitter<any> = new EventEmitter();
  columns = [
    {
      label: 'Issue ID',
      prop: 'iid',
      showing: true
    },
    {
      label: 'Description',
      prop: 'showDescription',
      showing: true
    },
    {
      label: 'Estimate',
      prop: 'showEstimated',
      showing: true
    },
    {
      label: 'Time Spent',
      prop: 'showSpent',
      showing: true
    },
    {
      label: 'Remaining',
      prop: 'showRemaining',
      showing: true
    }
  ];
  constructor() { }

  ngOnInit() {
  }

}
