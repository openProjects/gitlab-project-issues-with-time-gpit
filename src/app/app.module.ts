import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IssueRowComponent } from './issue-row/issue-row.component';
import { HttpClientModule } from '@angular/common/http';
import { SecondsToHumanReadablePipe } from './seconds-to-human-readable.pipe';
import { IssueHeaderRowComponent } from './issue-row/issue-header-row.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ColumnSelectorComponent } from './column-selector/column-selector.component';

@NgModule({
  declarations: [
    AppComponent,
    IssueRowComponent,
    IssueHeaderRowComponent,
    ColumnSelectorComponent,

    SecondsToHumanReadablePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BsDropdownModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
