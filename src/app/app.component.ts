import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { unparse } from 'papaparse';
import {saveAs} from 'file-saver'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'gitlab-time-estimate';
  items = [];
  projects = [];
  total: Number;
  serverUrl: string;
  privateToken: string;
  issueBaseUrl: string;
  loading = true;
  selectedProject;
  setupConnection = true;

  dataVisibility = {
    showDescription: true,
    showEstimated: true,
    showSpent: true,
    showRemaining: true
  };

  constructor( private http: HttpClient) { }

  ngOnInit() {
    this.fetchGitlabConfig();
  }

  columnsUpdated(columns) {
    console.log('columns updated');
    console.log(columns);
    columns.forEach((column) => {
      this.dataVisibility[column.prop] = column.showing;
    });
  }

  exportAsCSV() {
    console.log('exporting CSV')

    var mappedDataForExport = this.items.map((item)=>({
      ...item,
      ...item.milestone,
      ...item.assignee,
      ...item.author
    }))
    var exportData = unparse(mappedDataForExport)

    var blob = new Blob([exportData], {type: 'text/plain;charset=utf-8'})
    saveAs(blob, 'gitlab_issues.csv', {autoBom: true})
  }

  fetchGitlabConfig() {
    const configUrl = `assets/gitlab-config.json`;
    this.http.get(`${configUrl}`).subscribe( (data: any) => {
      console.log('here')
      this.privateToken = data.privateToken;
      this.serverUrl = data.serverUrl;
      this.fetchProjects();
    });
  }

  fetchProjects() {
    const projectsUrl = `${this.serverUrl}/api/v4/projects?per_page=100&membership=true`;
    this.http.get(`${projectsUrl}`, {
      headers: {
        'PRIVATE-TOKEN': this.privateToken
      }
    }).subscribe( (data) => {
      this.projects = data as Array<any>;
      this.selectedProject = this.projects[0];
      this.fetchIssues();
    });
  }

  fetchIssues() {
    this.issueBaseUrl = `${this.serverUrl}/${this.selectedProject.path_with_namespace}/issues/`;
    this.http.get(`${this.serverUrl}/api/v4/projects/${this.selectedProject.id}/issues?state=opened&per_page=100&page=1`, {
      headers: {
        'PRIVATE-TOKEN': this.privateToken
      }
    }).subscribe( (data) => {
      this.items = data as Array<any>;
      this.total = this.items.reduce((x, y) => x + y.time_stats.time_estimate, 0);
      this.loading = false;
      this.setupConnection = false;
    });
  }
}
